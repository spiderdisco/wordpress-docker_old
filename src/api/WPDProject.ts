import * as fs from 'fs';
import * as path from 'path';

export default class WPDProject {

	constructor(
		/** Absolute path to the project directory. */
		readonly dir: string,
	) {

		// Ensure `dir` is actually a directory.
		if(this.exists) {
			if(!fs.statSync(this.dir).isDirectory()) {
				throw new Error(`${this.dir} is not a directory.`);
			}
		}
	}

	get isEmpty() {
		if(this.exists) {
			return fs.readdirSync(this.dir).length === 0;
		}
		return true;
	}

	get exists() {
		return fs.existsSync(this.dir);
	}
}
