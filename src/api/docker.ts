import { spawn } from 'child_process';
import { inherits } from 'util';
import { WPDError } from './errors';


namespace docker {

	export function getVersion(): Promise<string | `not-installed`> {
		return new Promise((resolve, reject) => {
			const cp = spawn(`docker --version`, {
				shell: true,
			});
			cp.stdout.on(`data`, d => {
				resolve(d);
			});
			cp.on(`exit`, code => {
				if(code !== 0) {
					reject(new WPDError(`docker not installed`));
				}
			});
		});
	}

	export function isRunning(): Promise<boolean> {
		return new Promise((resolve, reject) => {
			const cp = spawn(`docker info`, {
				shell: true,
			});

			cp.on(`exit`, code =>  resolve(code === 0));
		});
	}

}
export default docker;
