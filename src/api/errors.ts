export type WPDErrorType =
	`docker not installed`
	|  `docker not running`
;

export class WPDError extends Error {
	readonly isWpd: true;
	constructor(
		readonly type: WPDErrorType,
	) {
		super(`wpd error: ${type}`);
	}
}
