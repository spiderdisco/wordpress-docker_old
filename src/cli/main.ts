import * as path from 'path';
import * as yargs from 'yargs';
import { WPDError } from './../api/errors';

import WPDProject from '../api';
import docker from '../api/docker';
import { getPackage, log, parseDirectoryArg } from './util';

(async () => {
	try {
		const wpdVersion = getPackage().version;
		log.tmi(`wpd version ${wpdVersion}`);

		const dockerVersion = await docker.getVersion();
		log.tmi(dockerVersion);

		if(!(await docker.isRunning())) {
			throw new WPDError(`docker not running`)
		}

	} catch(err) {
		if(err instanceof WPDError) {
			switch((err as WPDError).type) {
				case `docker not installed`:
					log.err(`Docker does not appear to be installed on your system.`);
					log.inf(`visit https://docs.docker.com/install/#supported-platforms to install docker for your platform.\n`);
					break;

				case `docker not running`:
					log.err(`Docker does not appear to be running.`);
					log.inf(`try launching it, or running \`dockerd\`.\n`);
					break;

			}
		} else {
			log.err(`unexpected error!`, err);
			console.log();
			log.inf(
(`this might be a bug with wordpress-docker. if you think so, please report it at
   https://gitlab.com/spiderdisco/wordpress-docker/issues
`),
			);
		}
	}


})();


// (async () => {
// 	///////////////////////////////////////////////////////////////////////////////
// 	// Setup
// 	///////////////////////////////////////////////////////////////////////////////
// 	const pkg =  getPackage();

// 	log.tmi(`wordpress-docker version ${pkg.version}`);

// 	let dockerVersion: string;
// 	try {
// 		dockerVersion = await docker.getVersion();
// 	} catch(error) {
// 		console.log(`no`);
// 		process.exit();
// 	}


// 	const args = yargs
// 	.scriptName(`wpd`)
// 	.version(pkg.version)
// 	.options({
// 		directory: {
// 			alias: `d`,
// 			default: null,
// 		},
// 	})
// 	.command(`init`, `start a new wpd project.`, {
// 		plugin: {
// 			alias: `p`,
// 			array: true,
// 			description: `one or more plugin names`,
// 		},
// 		theme: {
// 			alias: `t`,
// 			array: true,
// 			description: `one or more theme names`,
// 		},
// 	})
// 	.command(`up`, `serve a wpd project.`)
// 	.strict()
// 	.help()
// 	.argv
// 	;


// 	const directory = parseDirectoryArg(args.directory, process.cwd());
// 	log.tmi(directory);

// 	const project = new WPDProject(directory);

// 	if(args._.includes(`init`)) {

// 		const themes = args.theme || [];
// 		log.tmi(`themes: ${themes}`);

// 		const plugins = args.plugins || [];
// 		log.tmi(`plugs: ${plugins}`);


// 		const init = args._[`init`];
// 		log.tsk(`initialise`);
// 		console.log(args);

// 	} else if(args._.includes(`up`)) {

// 		log.tsk(`up`);
// 		console.log(args);
// 	}
// })();
