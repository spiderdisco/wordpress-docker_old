import chalk from 'chalk';

export namespace log {
	function out(icon: string, message: string, format: (x: string) => string) {
		console.log(format(` ${icon} ${message}`));
	}

	export function tmi(message: string) {
		out(`-`, message, chalk.dim);
	}

	export function inf(message: string) {
		out(`-`, message, chalk.white);
	}

	export function tsk(message: string) {
		out(`▶`, message, chalk.bold.cyanBright);
	}

	export function war(message: string) {
		out(`!`, message, chalk.yellow);
	}

	export function err(message: string, error?: Error) {
		out(`❌`, message, chalk.bold.yellowBright.bgRed);
		if(error) {
			error.stack.split(`\n`).forEach(l => out(` `, l, chalk.bold.redBright));
		}
	}
}
