import * as path from 'path';

export function parseDirectoryArg(arg: string | null, root: string): string {
	return arg ? path.resolve(root, arg) : root;
}
