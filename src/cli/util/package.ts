import * as fs from 'fs';
import * as path from 'path';


export function getPackage(): any {
	const packagePath = path.resolve(__dirname, `../../../package.json`);
	const packageJSON = fs.readFileSync(packagePath, `utf8`);
	return JSON.parse(packageJSON);
}
