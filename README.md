**wordpress-docker**
====================

[![pipeline status](https://gitlab.com/spiderdisco/wordpress-docker/badges/dev/pipeline.svg?style=flat-square)](https://gitlab.com/spiderdisco/wordpress-docker/commits/dev)
[![coverage report](https://gitlab.com/spiderdisco/wordpress-docker/badges/dev/coverage.svg?style=flat-square)](https://gitlab.com/spiderdisco/wordpress-docker/commits/dev)

**cli utility for local wordpress theme & plugin development**

----

