import * as fs from 'fs';
import * as path from 'path';


export namespace TestFiles {
	const ROOT = __dirname;

	export const EMPTY_DIRECTORY = path.join(ROOT, `empty-dir`);
	if(!fs.existsSync(EMPTY_DIRECTORY)) {
		fs.mkdirSync(EMPTY_DIRECTORY);
	}

	export const NON_EMPTY_DIRECTORY = path.join(ROOT, `non-empty-dir`);

	export const EXISTANT_DIRECTORY = NON_EMPTY_DIRECTORY;
	export const NON_EXISTANT_DIRECTORY = path.join(ROOT, `no-such-directory`);

	export const EXISTANT_FILE = path.join(NON_EMPTY_DIRECTORY, `anything.txt`);
}
