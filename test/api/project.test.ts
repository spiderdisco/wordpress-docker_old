// tslint:disable: no-unused-expression
import { expect } from 'chai';
import 'jasmine';

import { TestFiles } from '../helpers';

import WPDProject from '../../src/api';


describe(`WPDProject`, () => {

	describe(`ctor`, () => {
		it(`throws if passed an existant non-directory`, () => {
			expect(() => new WPDProject(TestFiles.EXISTANT_FILE))
				.to.throw(`is not a directory`);
		});
	});

	describe(`exists()`, () => {
		it(`returns true for an existant directory`, () => {
			expect(new WPDProject(TestFiles.EXISTANT_DIRECTORY).exists)
				.to.be.true;
		});

		it(`returns false for a non-existant directory`, () => {
			expect(new WPDProject(TestFiles.NON_EXISTANT_DIRECTORY).exists)
				.to.be.false;
		});
	});

	describe(`isEmpty()`, () => {
		it(`returns true for empty directory`, () => {
			expect(new WPDProject(TestFiles.EMPTY_DIRECTORY).isEmpty)
				.to.be.true;
		});

		it(`returns false for directory with children`, () => {
			expect(new WPDProject(TestFiles.NON_EMPTY_DIRECTORY).isEmpty)
				.to.be.false;
		});

		it(`returns true for non-existant directory`, () => {
			expect(new WPDProject(TestFiles.NON_EXISTANT_DIRECTORY).isEmpty)
				.to.be.true;
		});
	});
});
