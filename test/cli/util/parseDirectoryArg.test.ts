// tslint:disable: no-unused-expression
import { expect } from 'chai';
import 'jasmine';

import { parseDirectoryArg } from "../../../src/cli/util/parseDirectoryArg";

describe(`parseDirectoryArg()`, () => {
	it(`returns root if arg is null`, () => {
		expect(parseDirectoryArg(null, `some/where`))
			.to.eq(`some/where`);
	});

	it(`returns absolute path resolved to root if arg is relative path`, () => {
		expect(parseDirectoryArg(`rel/path`, `/abs/path`))
			.to.eq(`/abs/path/rel/path`);

		expect(parseDirectoryArg(`../going/up`, `/abs/path`))
			.to.eq(`/abs/going/up`);
	});

	it(`returns arg if arg is absolute path`, () => {
		expect(parseDirectoryArg(`/abs/path`, `/somewhere/else`))
			.to.eq(`/abs/path`);
	});
});
